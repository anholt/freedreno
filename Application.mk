APP_OPTIM := debug
APP_ABI := arm64-v8a armeabi-v7a
APP_STL := c++_static
APP_CPPFLAGS := -frtti -fexceptions
APP_PLATFORM := android-23
APP_BUILD_SCRIPT := Android.mk
APP_CFLAGS := -DBIONIC_IOCTL_NO_SIGNEDNESS_OVERLOAD -UFORTIFY_SOURCE -D_FORTIFY_SOURCE=0 -Wno-macro-redefined
